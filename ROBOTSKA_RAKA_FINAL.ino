#include <Stepper.h>
#include <Servo.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <LcdBarGraph.h>

LiquidCrystal lcd(1, 2, 4, 5, 6, 7); // (rs, enable, d4, d5, d6, d7)
LcdBarGraph lbg(&lcd, 16, 0, 1); 

#define STEPS 32
#define IN1  11 
#define IN2  10 
#define IN3   9 
#define IN4   8 

#define joystickX  A0 // servo motor ANALOG IN, pin A0
#define joystickY  A1 // step motor ANALOG IN, pin A1
Servo myservo;

#define max_distance 30

Stepper stepper(STEPS, IN4, IN2, IN3, IN1);


const int trigPin = 13;
const int echoPin = 12;
long duration;
int distanceCm;
int servoVal;

void setup()
{
  myservo.attach(3);
  lcd.begin(16,2);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop()
{
  //=============================================================================================================
  // SERVO MOTOR
  servoVal = analogRead(joystickX);
  servoVal = map(servoVal, 0, 1023, 0, 180);
  myservo.write(servoVal);
  delay(15);

  
  // ============================================================================================================
  // ULTRASONIC SENSOR + DISPLAY
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  distanceCm = duration*0.034/2;


  lcd.setCursor(0,0); 
  lcd.print("Distance: ");
  lcd.print(distanceCm);
  lcd.print("  cm");
  lcd.setCursor(0,1);
  lbg.drawValue(distanceCm, max_distance);
  delay(500);

  //=============================================================================================================
  // STEP (CEKOREN) MOTOR
  int stepVal = analogRead(joystickY);
  if(  (stepVal > 500) && (stepVal < 523) )
  {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW);
  }
  
  else
  {
    
    while (stepVal >= 523)
    {
      
      int speed_ = map(stepVal, 523, 1023, 5, 500);
      
      stepper.setSpeed(speed_);

      stepper.step(1);
 
      stepVal = analogRead(joystickY);
    }
 
    
    while (stepVal <= 500)
    {
      
      int speed_ = map(stepVal, 500, 0, 5, 500);
      stepper.setSpeed(speed_);
      stepper.step(-1);
      stepVal = analogRead(joystickY);
    }
 
  }
}